#Ejecutar docker rabbitmq
docker run -d --hostname rabbit1 --name rabbit1 -e RABBITMQ_ERLANG_COOKIE='rabbitcluster' -p 30000:5672 -p 30001:15672 rabbitmq:management

docker run -d --hostname rabbit2 --name rabbit2 --link rabbit1:rabbit1 -e RABBITMQ_ERLANG_COOKIE='rabbitcluster' -p 30002:5672 -p 30003:15672 rabbitmq:management

docker run -d --hostname rabbit3 --name rabbit3 --link rabbit1:rabbit1 --link rabbit2:rabbit2 -e RABBITMQ_ERLANG_COO`

1. docker exec -i -t rabbit2 \bash
2. root@rabbit2:/# rabbitmqctl stop_app
Stopping node rabbit@rabbit2 ...
3. root@rabbit2:/# rabbitmqctl join_cluster rabbit@rabbit1
Clustering node rabbit@rabbit2 with rabbit@rabbit1 ...
4. root@rabbit2:/# rabbitmqctl start_app
Starting node rabbit@rabbit2 ...


En el interior CachingConnectionFactory,  configuramos las tres direcciones de las instancias de cl�ster 
RabbitMQ:  192.168.99.100:30000,  192.168.99.100:30002,  192.168.99.100:30004. 

